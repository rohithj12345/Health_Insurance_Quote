package com.test;

import model.Habits;
import model.Health;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import service.PremiumGenerator;

public class PremiumGeneratorTest {

	/**
	 * Unit test class for testing PremiumGenerator class
	 */
	private PremiumGenerator premiumGenerator = new PremiumGenerator();
	
	@BeforeClass
	public static void start(){
		System.out.println("Before Class");
	}
	
	@Test
	public void testcalculatePremiumByAgeNotNull(){
		double actualoutput=premiumGenerator.calculatePremiumByAge(34);
		Assert.assertNotNull(actualoutput);
	}
	@Test
	public void testcalculatePremiumByGenderNotNull(){
		double actualoutput=premiumGenerator.calculatePremiumByGender("m");
		Assert.assertNotNull(actualoutput);
	}
	@Test
	public void testcalculatePremiumByHealthNotNull(){
		Health health = new Health();
		double actualoutput=premiumGenerator.calculatePremiumByHealth(health);
		Assert.assertNotNull(actualoutput);
	}
	@Test
	public void testcalculatePremiumByHabitsNotNull(){
		Habits habits =new Habits();
		double actualoutput=premiumGenerator.calculatePremiumByHabit(habits);
		Assert.assertNotNull(actualoutput);
	}
	
	@Test
	public void testcalculatePremiumByAge(){
		double actualoutput=premiumGenerator.calculatePremiumByAge(34);
		Assert.assertEquals(5500.0, actualoutput, 0);
	}
	@Test
	public void testcalculatePremiumByGender(){
		double actualoutput=premiumGenerator.calculatePremiumByGender("m");
		Assert.assertEquals(100.0, actualoutput, 0);
	}
	@Test
	public void testcalculatePremiumByHabit(){
		Habits habits =new Habits();
		double actualoutput=premiumGenerator.calculatePremiumByHabit(habits);
		Assert.assertFalse(actualoutput==1.0);
	}
	@Test
	public void testcalculatePremiumByHealth(){
		Health health = new Health();
		double actualoutput=premiumGenerator.calculatePremiumByHealth(health);
		Assert.assertTrue(actualoutput==0.0);
	}
	
	@AfterClass
	public static void end(){
		System.out.println("After Class");
	}
}

package controller;

import model.Customer;
import model.Habits;
import model.Health;
import service.PremiumGenerator;

public class Test {

	/**
	 * In this class we can set user inputs and calculate the final premium
	 */
	public static void main(String[] args) {
		
		Test test = new Test();
		Customer customerDetails = test.getCustomerDetails();
		
		PremiumGenerator premiumGenerator = new PremiumGenerator();
		double finalpremium = premiumGenerator.calculatePremium(customerDetails);
		System.out.println("Health Insurance Premium for "+customerDetails.getName()+ " is Rs:"+finalpremium);

	}
	
	public Customer getCustomerDetails(){
		Customer customer = new Customer();
		customer.setName("Norman Gomes");
		customer.setGender("male");
		customer.setAge(34);
		
		Health health = new Health();
		health.setOverWeight(true);

		Habits habits = new Habits();
		habits.setDailyExersice(true);
		habits.setAlcohol(true);
		
		customer.setHealth(health);
		customer.setHabits(habits);
		
		return customer;
	}

}

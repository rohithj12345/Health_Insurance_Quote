package model;

import java.io.Serializable;

public class Customer implements Serializable{

	/**
	 * This class holds the Customer details
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String gender;
	private int age;
	private Health health;
	private Habits habits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Health getHealth() {
		return health;
	}
	public void setHealth(Health health) {
		this.health = health;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
}

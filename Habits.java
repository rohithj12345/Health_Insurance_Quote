package model;

import java.io.Serializable;

public class Habits implements Serializable{

	/**
	 * This class holds the Customer Habits
	 */
	private static final long serialVersionUID = 1L;
	private boolean smoking;
	private boolean alcohol;
	private boolean dailyExersice;
	private boolean drugs;
	
	public boolean isSmoking() {
		return smoking;
	}
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	public boolean isAlcohol() {
		return alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	public boolean isDailyExersice() {
		return dailyExersice;
	}
	public void setDailyExersice(boolean dailyExersice) {
		this.dailyExersice = dailyExersice;
	}
	public boolean isDrugs() {
		return drugs;
	}
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	

}

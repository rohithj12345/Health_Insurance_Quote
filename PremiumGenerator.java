package service;

import model.Customer;
import model.Habits;
import model.Health;

public class PremiumGenerator {

	/**
	 * This class has premium generation logic based on gender,age,health condition
	 * and habits of the customer
	 */
	
	private double basePremium = 5000;
		
	public double calculatePremiumByAge(int age) {
		if(age<18){
			return basePremium;
		}else if(age >= 18 && age <= 25){
			basePremium =basePremium + (basePremium *0.1);	
		}else if(age >25 && age <= 30){
			basePremium =basePremium + (basePremium *0.1);		
		}else if(age >30 && age <= 35){
			basePremium =basePremium + (basePremium *0.1);
		}else if(age >35 && age <= 40){
			basePremium =basePremium + (basePremium *0.1);		
		}else if(age >40){
			basePremium =basePremium + (basePremium *0.2);		
		}
		return basePremium;
	}
	
	public double calculatePremiumByGender(String gender) {
		if("male".equalsIgnoreCase(gender)){
			return basePremium * 2/100;
		}
		return 0.0;
	}
	
	public double calculatePremiumByHealth(Health health) {
		double increasePercent = 0.0;
		increasePercent = increasePercent + (health.isHyperTension()? 1 : 0) ;
		increasePercent += health.isBloodPressure()? 1 : 0 ;
		increasePercent += health.isBloodSugar()? 1 : 0 ;
		increasePercent += health.isOverWeight() ? 1 : 0 ;
		
		return basePremium * (increasePercent/100);
	}
	
	public double calculatePremiumByHabit(Habits habits) {
		double increasePercent = 0.0;

		increasePercent = increasePercent + (habits.isSmoking() ? 1 : 0);
		increasePercent += habits.isAlcohol() ? 1 : 0;
		increasePercent -= habits.isDailyExersice() ? 1 : 0;
		increasePercent += habits.isDrugs() ? 1 : 0;

		return basePremium * (increasePercent * 3 / 100);
	}
	
	public double calculatePremium(Customer customer) {
		double totalPremiumByAge = calculatePremiumByAge(customer.getAge());
		double totalPremiumByGender = calculatePremiumByGender(customer.getGender());
		double totalPremiumByHealth = calculatePremiumByHealth(customer.getHealth());
		double totalPremiumByHabit = calculatePremiumByHabit(customer.getHabits());
		System.out.println("totalPremiumByAge-->"+totalPremiumByAge);
		System.out.println("totalPremiumByGender-->"+totalPremiumByGender);
		System.out.println("totalPremiumByHealth-->"+totalPremiumByHealth);
		System.out.println("totalPremiumByHabit-->"+totalPremiumByHabit);
		
		return totalPremiumByAge+totalPremiumByGender+totalPremiumByHealth+totalPremiumByHabit;
		
	}
}

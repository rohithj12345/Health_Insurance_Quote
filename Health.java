package model;

import java.io.Serializable;

public class Health implements Serializable{

	/**
	 * This class hold the health details of Customer
	 */
	private static final long serialVersionUID = 1L;
	private boolean hyperTension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;
	
	public boolean isHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(boolean hyperTension) {
		this.hyperTension = hyperTension;
	}
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public boolean isOverWeight() {
		return overWeight;
	}
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}

}
